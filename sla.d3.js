/*global
 jQuery,
 d3,
 nv,
 saveSvgAsPng,
 createModal
 */

/**
 * Settings are stored in a JSON object keyed on chart ids, with each
 * respective value storing the chart's associated parameters.
 *
 * e.g.:
 *
 *   {
 *    "<chart_id_0>" : {
 *      "colors" : ["5e5e5e"],
 *      "..." : ...
 *    },
 *
 *         ...
 *
 *    "<chart_id_n>" : {
 *      "colors" : ["5e5e5e"],
 *      "..." : ...
 *    }
 *  }
 */
/**
 * This library depends on:
 *
 * jQuery
 * d3
 * nvd3
 * saveSvgAsPng
 *
 * and thus is implemented as a global object called sla, and not as an
 * addition to the global d3 object. This preserves the d3 namespace and
 * asserts sla as a library on top of d3, etc.
 *
 * To draw a chart, call sla.<chartName> with the appropriate parameters.
 */

// global sla library
var sla;

(function ($) {
    // context (document) and the global JS settings object
    // the global sla library object
    sla = {};
    sla.settings = {};

    /*
     d3.json(
     // sla-specific settings are nested under sla
     settings.sla.path,
     function (data) {
     sla.settings = data;
     }
     );
     */

    function formatValue(value) {
        'use strict';
        value = value / 1e6;
        return ' $' + (value < 0.1 ? '<0.1' : value.toFixed(value < 1 ? 2 : 0)) + 'M';
    }

    function setSLAChart(id, settings) {
        /* Set the container for an SLA chart based on the given element id.
         *
         */
        'use strict';
        var defaultWidth = '100%';
        var defaultHeight = '100%';

        var setting;
        for (setting in settings) {
            if (typeof settings[setting] === 'number') {
                settings[setting] = settings[setting] + 'px';
            }
        }

        //Create SVG element
        var svg = d3.select('#' + id)
            .append("svg")
            .attr("id", id + '-chart')
            .style('width', settings.w || defaultWidth)
            .style('height', settings.h || defaultHeight);

        return svg;
    }

    function createModal(id, data, settings) {
        'use strict';

        var content = d3.select('.content');
        var title = d3.select('#' + id + ' h3').text();
        var thumbs = d3.select('#thumbs')
            .classed('background', true);

        debugger;
        
        var div = content.append('div')
            .attr('id', id + '-full')
            .attr('class', 'chart-full');

        div.append('h3').text(title);
        // add a close button
        div.append('button')
            .text('Close')
            .attr('class', 'btn btn-default close-sla')
            .attr('aria-label', 'Close')
            .on('click', function () {
                div.remove();
                thumbs.classed('background', false);
            });
        // add a save button
        div.append('button')
            .text('Save chart')
            .attr('class', 'btn btn-default save-as-png')
            .attr('aria-label', 'Save as .png')
            .on('click', function () {
                var chart = d3.select('#' + id + '-full-chart')[0][0];
                var outputFilename = id + '-chart.png';
                var settings = {backgroundColor: 'white'};

                saveSvgAsPng(chart, outputFilename, settings);
            });

        if (settings.type === 'drawVBar') {
            sla.drawVBar(id + '-full', data, settings);
        } else if (settings.type === 'drawDonut') {
            sla.drawDonut(id + '-full', data, settings);
        } else if (settings.type === 'drawHBar') {
            sla.drawHBar(id + '-full', data, settings);
        } else if (settings.type === 'drawVBarLine') {
            sla.drawVBarLine(id + '-full', data, settings);
        } else if (settings.type === 'drawVBarFocus') {
            sla.drawVBarFocus(id + '-full', data, settings);
        }
    }

    // calls donut chart and passes settings from search-cases-d3.js
    sla.drawDonut = function drawDonut(id, data, settings) {
        'use strict';

        nv.addGraph(function () {
            var div = d3.select('#' + id);

            if (div.classed('chart-thumb')) {
                var fullSettings = jQuery.extend(true, {}, settings);
                fullSettings.type = 'drawDonut';
                if (settings.thumbSettings === undefined) {
                    settings.margin = {top: 0, left: 0, right: 0, bottom: 0};
                    settings.showLabels = false;
                    settings.containerHeight = '80%';
                    settings.noLegend = true;
                } else {
                    var setting;
                    for (setting in settings.thumbSettings) {
                        settings[setting] = settings.thumbSettings[setting];
                    }
                }

                div.on('click', function () {
                    createModal(id, data, fullSettings);
                });
            }

            var svg = setSLAChart(id, {w: settings.containerWidth, h: settings.containerHeight});
            var chart = settings.chart;
            var colors = settings.colors || d3.scale.category20().range();

            if (!chart) {
                chart = nv.models.pieChart()
                    .x(function (d) {
                        return d.label;
                    })
                    .y(function (d) {
                        return d.value;
                    })
                    .showLabels(true)
                    .donut(true)
                    //.valueFormat(d3.format('d'))
                    .labelType(function (d, i, labelTypes) {
                        return labelTypes.key + ': ' + parseInt(labelTypes.percent.split('.')[1]) + '%';
                    })
                    .labelsOutside(true);

                // this changes tooltip values to integers
                chart.tooltip.valueFormatter(function (d) {
                    return d;
                });

                if (settings.title) {
                    chart = chart.title(settings.title);
                }
            }

            chart.color(nv.utils.getColor(colors));

            if (settings.noLegend === true || settings.noLegend === undefined) {
                chart.showLegend(false);
            } else {
                chart.showLegend(true);
            }

            if (settings.margin) {
                // default margins are {top: 30, right: 20, bottom: 50, left: 60}
                chart.margin(settings.margin);
            }

            if (settings.showLabels === false) {
                chart.showLabels(false);
            }
            // .nvd3 .nv-legend .nv-series
            d3.select('#' + id + '-chart')
                .datum(data)
                .transition().duration(1200)
                .call(chart);

            if (settings.noClickableLegend === true || settings.noClickableLegend === undefined) {
                chart.legend.updateState(false);
                svg.selectAll('.nv-series').style('cursor', 'unset');
            }
            nv.utils.windowResize(chart.update);

            // resize lawsuits-by-market-cap
            d3.select('#lawsuits-by-market-cap-full-chart')
                .select('.nv-pieLabels')
                .selectAll('text')
                .style('font-size', '15px');

            return chart;
        });
    };

    sla.drawFixedLineAndText = function drawFixedLineAndText(id, yValue, text, chart) {
        'use strict';
        var yValueScale = chart.yAxis.scale();
        var xValueScale = chart.xAxis.scale();

        var domain = xValueScale.domain();
        var maxx = domain[domain.length - 1];
        var margin = chart.margin();
        var svg = d3.select("#" + id + "-chart");

        svg.append("line")
            .style("stroke", "#FF7F0E")
            .style("stroke-width", "2.5px")
            .attr("x1", margin.left)
            .attr("y1", yValueScale(yValue) + margin.top)
            .attr("x2", xValueScale(maxx) + margin.left)
            .attr("y2", yValueScale(yValue) + margin.top);

        //add text to fixed line
        d3.select("#" + id + "-chart")
            .append("text")
            .attr("x", margin.left + 30)
            .attr("y", yValueScale(yValue) + margin.top - 10)
            .attr("text-anchor", "middle")
            .text(text);
        //end fixed line
    };

    // calls horizontal bar chart and passes settings from search-cases-d3.js
    sla.drawHBar = function drawHBar(id, data, settings) {
        /* Create a horizontal bar chart, taking the dataset, id and settings.

         Create a horizontal bar chart within the div specified by parameter 'id',
         using the dataset in parameter 'data' and settings in parameter 'settings'.

         If the class of the div is 'chart-thumb', this function creates a thumbnail
         chart with on-click listener set to call createModal(). Note: This creates a
         circular dependency with createModal().

         Otherwise, this function creates a full-size chart.
         */
        'use strict';

        nv.addGraph(function () {
            var div = d3.select('#' + id);

            if (div.classed('chart-thumb')) {
                var fullSettings = jQuery.extend(true, {}, settings);
                fullSettings.type = 'drawHBar';
                if (settings.thumbSettings === undefined) {
                    settings.noXAxis = true;
                    settings.noYAxis = true;
                    settings.margin = {top: 0, left: 0, right: 0, bottom: 0};
                    settings.containerHeight = '80%';
                    settings.noLegend = true;
                } else {
                    var setting;
                    for (setting in settings.thumbSettings) {
                        settings[setting] = settings.thumbSettings[setting];
                    }
                }
                div.on('click', function () {
                    createModal(id, data, fullSettings);
                });
            }

            var svg = setSLAChart(id, {w: settings.containerWidth, h: settings.containerHeight});
            var chart = settings.chart;
            var colors = settings.colors || d3.scale.category20().range();

            // default chart parameters if a chart isn't declared in the settings
            if (!chart) {
                chart = nv.models.multiBarHorizontalChart()
                    .x(function (d) {
                        return d.label;
                    })
                    .y(function (d) {
                        return d.value;
                    })
                    .margin(settings.margin || {top: 0, right: 20, bottom: 50, left: 50})
                    .showValues(true)           //Show bar value next to each bar.
                    .tooltips(true)             //Show tooltips on hover.
                    .showYAxis(false)
                    //.showControls(typeof settings.showControls !== 'undefined' ? settings.showControls : false)        //Allow user to switch between "Grouped" and "Stacked" mode.
                    .showControls(settings.showControls !== 'undefined' ? settings.showControls : false)
                    .showLegend(false)
                    .color(nv.utils.getColor(colors));

                chart.xAxis
                    .tickFormat(function (d) {
                        return typeof d === 'number' ? d3.format(settings.xformat || 'f')(d) : d;
                    });

                chart.yAxis
                    .tickFormat(function (d) {
                        return d3.format(settings.yformat || 'f')(d).replace('G', 'B');
                    });
            }

            chart.valueFormat(function (d) {
                return d3.format(settings.yformat || 'f')(d).replace('G', 'B');
            });


            if (settings.yAxisLabel) {
                chart.yAxis.axisLabel(settings.yAxisLabel);
            }

            if (settings.xAxisLabel) {
                chart.xAxis.axisLabel(settings.xAxisLabel);
            }

            if (settings.noLegend) {
                chart.showLegend(false);
            } else {
                chart.showLegend(true);
            }

            if (settings.noXAxis) {
                chart.showXAxis(false);
            } else {
                chart.showXAxis(true);
            }

            if (settings.margin) {
                // default margins are {top: 30, right: 20, bottom: 50, left: 60}
                chart.margin(settings.margin);
            }

            d3.select('#' + id + '-chart')
                .datum(data)
                .call(chart);

            if (settings.noClickableLegend) {
                chart.legend.updateState(false);
                svg.selectAll('.nv-series').style('cursor', 'unset');
            }

            nv.utils.windowResize(chart.update);
            jQuery('#' + id).on('madeVisible', function () {
                console.log('made visible');
                chart.update();
            });

            return chart;
        });
    };

    sla.drawVBar = function drawVBar(id, data, settings) {
        'use strict';

        nv.addGraph(function () {
            var div = d3.select('#' + id);

            if (div.classed('chart-thumb')) {
                var fullSettings = jQuery.extend(true, {}, settings);
                fullSettings.type = 'drawVBar';

                if (settings.thumbSettings === undefined) {
                    settings.noXAxis = true;
                    settings.noYAxis = true;
                    settings.margin = {top: 10, left: 0, right: 0, bottom: 0};
                    settings.containerHeight = '80%';
                    settings.noLegend = true;
                } else {
                    var setting;
                    for (setting in settings.thumbSettings) {
                        settings[setting] = settings.thumbSettings[setting];
                    }
                }
                div.on('click', function () {
                    createModal(id, data, fullSettings);
                });
            }

            var svg = setSLAChart(id, {w: settings.containerWidth, h: settings.containerHeight});
            var chart = settings.chart;
            var colors = settings.colors || d3.scale.category20().range();

            if (!chart) {
                chart = nv.models.multiBarChart()
                //.transitionDuration(350)
                    .reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
                    .rotateLabels(0)      //Angle to rotate x-axis labels.
                    .showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
                    .groupSpacing(0.1);  //Distance between each group of bars.

                chart.yAxis
                    .tickFormat(function (d) {
                        return d3.format(settings.yformat || 'f')(d).replace('G', 'B');
                    });
                
                if (settings.noLegend) {
                    chart.showLegend(false);
                } else {
                    chart.showLegend(true);
                }
            }

            // each data stream is one represented by one color in the array
            chart.color(nv.utils.getColor(colors));

            if (settings.yAxisLabel) {
                chart.yAxis.axisLabel(settings.yAxisLabel);
            }

            if (settings.xAxisLabel) {
                chart.xAxis.axisLabel(settings.xAxisLabel);
            }


            if (settings.noXAxis) {
                chart.showXAxis(false);
            } else {
                chart.showXAxis(true);
            }

            if (settings.noYAxis) {
                chart.showYAxis(false);
            } else {
                chart.showYAxis(true);
            }
            if (settings.margin) {
                // default margins are {top: 30, right: 20, bottom: 50, left: 60}
                chart.margin(settings.margin);
            }

            if (settings.customTooltip) {
                chart.tooltip.contentGenerator(function (datum) {
                    var titleString = '',
                        title = datum.data.title;

                    if (title) {
                        titleString += '<p class="title">' + title + '</p><p>';
                    }
                    /*
                     if (mediator) {
                     titleString += 'Mediator: ' + mediator + '<br/>';
                     }
                     */
                    titleString += 'Settlement amount: ' + formatValue(datum.data.y) + '<br/>';
                    if (datum.data.field_nature_of_misstatement) {
                        titleString += 'Nature of misstatement: ' +
                            (datum.data.field_nature_of_misstatement === "FI" ? "Financial" : "Non-Financial") + '<br/>';
                    }

                    if (datum.data.field_resolution_phase) {
                        titleString += 'Resolution phase: ' + datum.data.field_resolution_phase + '<br/>';
                    }
                    //      titleString += 'Amount paid by insurer:' + formatValue(data[1].values[i].y) + '<br/>';
                    return titleString;
                });
            }

            d3.select('#' + id + '-chart')
                .datum(data)
                .call(chart);

            if (settings.noClickableLegend) {
                chart.legend.updateState(false);
                svg.selectAll('.nv-series').style('cursor', 'unset');
            }

            if (settings.labels) {
                var labels = svg.select('.nv-x').selectAll('text');
                labels.style('fill', settings.labels.color);
                labels.style('font-size', settings.labels.size);
            }

            nv.utils.windowResize(function () {
                chart.update();
                if (settings.lines) {
                    console.log('resizing line labels... ');
                    //         drawLineLabels(chart, svg, settings.lines);
                }
            });

            // increase title size slightly
            d3.select('#number-filings-per-year-full').select('h3').style('font-size', '20px');

            return chart;
        });
    };

    sla.drawVBarFocus = function drawVBarFocus(id, data, settings) {
        'use strict';

        nv.addGraph(function () {
            var div = d3.select('#' + id);

            if (div.classed('chart-thumb')) {
                var fullSettings = jQuery.extend(true, {}, settings);
                fullSettings.type = 'drawVBarFocus';

                if (settings.thumbSettings === undefined) {
                    settings.noXAxis = true;
                    settings.noYAxis = true;
                    settings.margin = {top: 10, left: 0, right: 0, bottom: 0};
                    settings.containerHeight = '80%';
                    settings.noLegend = true;
                    settings.focusEnable = false;
                } else {
                    var setting;
                    for (setting in settings.thumbSettings) {
                        settings[setting] = settings.thumbSettings[setting];
                    }
                }

                div.on('click', function () {
                    createModal(id, data, fullSettings);
                });
            }

            var svg = setSLAChart(id, {w: settings.containerWidth, h: settings.containerHeight});
            var chart = nv.models.barChartFocus();

            chart.y1Axis
                .tickFormat(function (d) {
                    return d3.format(settings.yformat || 'f')(d).replace('G', 'B');
                });

            chart.xAxis
                .tickFormat(function (d) {
                    return d3.format(settings.xformat || 'f')(d);
                });

            chart.bars.forceY([0]);

            chart.legendLeftAxisHint('');
            chart.legendRightAxisHint('');

            if (settings.focusEnable === false) {
                chart.focusEnable(false);
            }
            if (settings.yAxisLabel) {
                chart.y1Axis.axisLabel(settings.yAxisLabel);
            }

            if (settings.xAxisLabel) {
                chart.x1Axis.axisLabel(settings.xAxisLabel);
            }

            if (settings.noLegend) {
                chart.showLegend(false);
            } else {
                chart.showLegend(true);
            }

            if (settings.noXAxis === true) {
                chart.showAxisX(false);
            } else {
                chart.showAxisX(true);
            }
            // if (settings.noYAxis) {
            //     chart.focusShowAxisY(false);
            // } else {
            //     chart.focusShowAxisY(true);
            // }

            if (settings.margin) {
                // defaults
                // margin = {top: 30, right: 30, bottom: 30, left: 60}
                // , margin2 = {top: 0, right: 30, bottom: 20, left: 60}
                chart.margin(settings.margin);
                // chart.margin( {top: settings.margin.top || 30, right: settings.margin.right || 30, left: settings.margin.left || 30} );
                // chart.margin2( {right: settings.margin.right || 30, left: settings.margin.left || 30, bottom: settings.margin.bottom || 20} );
            }

            if (settings.colors) {
                if (Array.isArray(settings.colors)) {
                    chart.color(settings.colors);
                }
                chart.bars.color(settings.colors);
                chart.bars2.color(settings.colors);
            }

            if (settings.clickableBars) {
                chart.bars.dispatch.on('elementClick', function (e) {
                    window.location = "/node/" + e.data.nid;    // link bar to the node
                });
            }

            chart.tooltip.contentGenerator(function (datum) {
                if (settings.colors) {
                    datum.color = typeof settings.colors === 'function' ? settings.colors(datum.data) : settings.colors[0];
                }

                var titleString = '';
                var title = datum.data.title;
                if (title) {
                    titleString += '<p class="title">' + title + '</p><p>';
                }
                titleString += 'Settlement amount: ' + d3.format('$.3s')(datum.data.y).replace('G', 'B') + '<br/>';
                if (datum.data.field_nature_of_misstatement) {
                    titleString += 'Nature of misstatement: ' +
                        (datum.data.field_nature_of_misstatement === "FI" ? "Financial" : "Non-Financial") + '<br/>';
                }
                if (datum.data.field_resolution_phase) {
                    titleString += 'Resolution phase: ' + datum.data.field_resolution_phase + '<br/>';
                }
                //      titleString += 'Amount paid by insurer:' + formatValue(data[1].values[i].y) + '<br/>';
                return titleString;
            });

            d3.select('#' + id + '-chart')
                .datum(data)
                .transition()
                .duration(0)
                .call(chart);

            if (settings.noClickableLegend) {
                chart.legend.updateState(false);
                svg.selectAll('.nv-series').style('cursor', 'unset');
            }

            nv.utils.windowResize(chart.update);

            return chart;
        });
    };

    sla.drawVBarLine = function drawVBarLine(id, data, settings) {
        'use strict';

        nv.addGraph(function () {
            var div = d3.select('#' + id);

            if (div.classed('chart-thumb')) {
                var fullSettings = jQuery.extend(true, {}, settings);
                fullSettings.type = 'drawVBarLine';
                if (settings.thumbSettings === undefined) {
                    settings.noXAxis = true;
                    settings.noYAxis = true;
                    settings.margin = {top: 10, left: 0, right: 0, bottom: 0};
                    settings.containerHeight = '80%';
                    settings.noLegend = true;
                } else {
                    var setting;
                    for (setting in settings.thumbSettings) {
                        settings[setting] = settings.thumbSettings[setting];
                    }
                }

                div.on('click', function () {
                    console.log(settings);
                    createModal(id, data, fullSettings);
                });
            }

            setSLAChart(id, {w: settings.containerWidth, h: settings.containerHeight});
            var chart = nv.models.multiBarChartLine().reduceXTicks(false);
            var colors = settings.colors || d3.scale.category20().range();

            chart.color(nv.utils.getColor(colors));

            /*
             chart.xAxis
             .tickFormat(function (d) {
             if (typeof d === 'number' && d / 1000 >= 1 && d / 1000 <= 3) {
             if (d % 2 === 0) {
             return d3.format(settings.xformat || 'f')(d);
             }
             return "";
             }
             return d;
             });
             */

            chart.yAxis
                .tickFormat(function (d) {
                    return d3.format(settings.yformat || 'f')(d).replace('G', 'B');
                });

            if (settings.showControls !== true) {
                chart.showControls(false);
            }

            if (settings.yAxisLabel) {
                chart.yAxis.axisLabel(settings.yAxisLabel);
            }

            if (settings.xAxisLabel) {
                chart.xAxis.axisLabel(settings.xAxisLabel);
            }

            if (settings.noLegend) {
                chart.showLegend(false);
            } else {
                chart.showLegend(true);
            }

            if (settings.noXAxis) {
                chart.showXAxis(false);
            } else {
                chart.showXAxis(true);
            }
            if (settings.noYAxis) {
                chart.showYAxis(false);
            } else {
                chart.showYAxis(true);
            }

            if (settings.margin) {
                // default margins are {top: 30, right: 20, bottom: 50, left: 60}
                chart.margin(settings.margin);
            }

            if (settings.clickableBars) {
                chart.multibar.dispatch.on('elementClick', function (e) {
                    window.location = "/node/" + e.data.nid;    // link bar to the node
                });
            }

            d3.select('#' + id + '-chart')
                .datum(data)
                .transition()
                .duration(0)
                .call(chart);

            nv.utils.windowResize(chart.update);

            return chart;
        });
    };

    sla.setSLAChart = setSLAChart;
}(jQuery));
